<!DOCTYPE html>
<html>
<head>
    <#include "../common/head.ftl"/>
    <!-- DataTable CSS -->
    <link rel="stylesheet" href="/plugin/AdminLTE/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="/plugin/AdminLTE/plugins/datepicker/datepicker3.css">
</head>
<body>
<div id="page-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            极光推送
            <small>&nbsp;</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/main"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li class="active">极光推送</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <form id="form1" name="form1"  >
        <!-- box -->
        <div class="box box-primary margin-bottom-none">
            <div class="box-body with-border">
                <!-- SearchTable -->
                <table id="searchTable" class="table table-bordered table-striped table-search">
                    <tr>
                        <td style="width:13%;"><span style="color:red">*&nbsp;</span>通知标题(Android专用)：</td>
                        <td><input type="text" class="form-control" name="title" value=""/></td>
                    </tr>
                    <tr>
                        <td style="width:10%;"><span style="color:red">*&nbsp;</span>通知内容：</td>
                        <td><textarea class="form-control" name="alert" id="alert" rows="5" style="resize:none;"></textarea></td>
                    </tr>
                    <tr>
                        <td rowspan="2" style="width:10%;"></span>自定义消息(标题/内容)：</td>
                        <td><input type="text" class="form-control" name="extras_key" id="extras_key" value=""/></td>
                    </tr>
                    <tr>
                        <td><textarea class="form-control" name="extras_value" id="extras_value" rows="5" style="resize:none;"></textarea></td>
                    </tr>

                    <tr>
                        <td style="width:10%;"><span style="color:red">*&nbsp;</span>目标平台（必选）：</td>
                        <td>
                            <input type="radio" name="goal_terrace"  value="2" checked="checked" >全部</input>
                            <input type="radio" name="goal_terrace"  value="0" >安卓</input>
                            <input type="radio" name="goal_terrace"  value="1" >IOS</input>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:10%;"><span style="color:red">*&nbsp;</span>目标人群：</td>
                        <td>
                                <input type="radio"  name="goal_people" class="goalPeople" checked="checked" value="0">广播(所有人)</input>
                                <input type="radio"  name="goal_people" class="goalPeople" value="1">设备别名(Alias)</input>
                        </td>
                    </tr>
                    <tr id="tr_alia">
                        <td data-toggle="tooltip" title="有效的 alias 组成：字母（区分大小写）、数字、下划线、汉字、特殊字符@!#$&*+=.|￥。" data-placement="right"><span style="color:red">*&nbsp;</span><strong>设备别名(Alias):</strong></td>
                        <td >
                            <font color="red">注:支持多个别名群发用英文逗号隔开</font><textarea  class="form-control" type="text" id="alias"  name="alias"   rows="5" style="resize:none;"></textarea>
                        </td>
                    </tr>
                </table>
                <!-- Buttons -->
                <div class="box-body btn-wrapper">
                    <button type="button" id="btn_add" class="btn btn-success btn-control"></i>&nbsp;立即发送</button>
                </div>

                <!-- DataTable -->
                <table id="dataTable" class="table data-table table-bordered table-striped" class="col-lg-12">

            </div>
        </div>
        </form>
    </section>
    <!-- /.content -->
</div>
<!-- REQUIRED JS SCRIPTS -->
<#include "../common/script-common.ftl"/>
<!-- DataTable -->
<script src="/plugin/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugin/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="/js/cdl-datatable.js"></script>
<!-- DatePicker -->
<script src="/plugin/AdminLTE/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="/plugin/AdminLTE/plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
<script> $.extend($.fn.datepicker.defaults, { language: 'zh-CN' }); </script>

<!-- jQueryValidate -->
<script src="/plugin/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/plugin/jquery-validation/dist/localization/messages_zh.js"></script>
<script src="/js/cdl-validate.js"></script>

<script>
    $(function () {
        alisHideAndShow();
    });
    $('.goalPeople').click(function () {
        alisHideAndShow();
    });
    function alisHideAndShow() {
        var val=$('input:radio[name="goal_people"]:checked').val();
        if(val==0){
            $('#tr_alia').hide();
        }else {
            $('#tr_alia').show();
        }
    }

    $("#btn_add").click(function () {
        var goalTerrace=$('input:radio[name="goal_terrace"]:checked').val();
        var goalPeople=$('input:radio[name="goal_people"]:checked').val();
        var title=$('input:text[name="title"]').val();
        var extras_key=$('#extras_key').val();
        var extras_value=$('#extras_value').val();

        function ObjData(key,value){
            this.Key=key;
            this.Value=value;
        }
        var map=new ObjData(extras_key,extras_value)
        $.ajax({
            type: "post",
            url: "/push/all",
            data: {
                "${_csrf.parameterName?default('_csrf')}": "${_csrf.token?default('')}",
                title:title,
                alert:$('#alert').val(),
                audience:goalTerrace,
                goalPeople:goalPeople,
                alias:$('#alias').val(),
                extras:map
            },
            dataType: "json",
            success: function(data){
                if(data.code===1){
                    toastr.error(data.message, "提示信息");
                }else if(data.code===0){
                    toastr.success(data.message, "提示信息");
                }
            },
            error: ajaxErrorHandler
        });
    });

</script>
</body>
</html>