package com.jykj.reward.web.general;


import cn.jpush.api.push.PushResult;
import com.jykj.reward.common.AsyncResult;
import com.jykj.reward.common.consts.Constant;
import com.jykj.reward.domain.Push;
import com.jykj.reward.domain.User;
import com.jykj.reward.service.jdpush.PushService;
import com.jykj.reward.service.system.UserService;
import com.jykj.reward.util.IdentityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/push")
public class PushController {
	
	@Autowired
	private PushService pushService;
    @Autowired
	private UserService userService;

	@RequestMapping("/index")
	public String index(Map<String, Object> model){

		return "/app/app_jpush";
	}



	/**
	 * 推送全部(包括ios和android)
	 * @param
	 * @return
	 */
	/*@RequestMapping(value="/all", method= RequestMethod.POST)
    @ResponseBody
	public boolean pushAll(Push pushBean) {
	    pushBean.setTitle("这是标题");
	    pushBean.setAlert("这是内容");
	    return pushService.pushAll(pushBean);
	}

	*//**
	 * 推送全部ios
	 * @param pushBean
	 * @return
	 *//*
	@RequestMapping(value="/ios/all", method= RequestMethod.POST)
	public boolean pushIos(Push pushBean){
		return pushService.pushIos(pushBean);
	}

	*//**
	 * 推送指定ios
	 * @param pushBean
	 * @return
	 *//*
	@RequestMapping(value="/ios", method= RequestMethod.POST)
	public boolean pushIos(Push pushBean, String[] registerids){
		return pushService.pushIos(pushBean, registerids);
	}

	*//**
	 * 推送全部android
	 * @param pushBean
	 * @return
	 *//*
	@RequestMapping(value="/android/all", method= RequestMethod.POST)
	@ResponseBody
	public boolean pushAndroid(Push pushBean){
		return pushService.pushAndroid(pushBean);
	}
	*/
	/**
	 * 推送指定android
	 * @param pushBean
	 * @return
	 */
	//@RequestMapping(value="/android", method= RequestMethod.POST)
	@PostMapping("/all")
	@ResponseBody
	public AsyncResult pushAndroid(Push pushBean, String alias){
		AsyncResult asyncResult = AsyncResult.error("推送失败");
		try {
            alias = alias == null ? "" : alias;
            String[] idArr = alias.split(",");
            List<String> mobileList= Arrays.asList(idArr);
            List<Integer> byMobile = userService.findByMobileList(mobileList);
            if(byMobile==null&&byMobile.size()<=0){
                return AsyncResult.error("手机号码不存在");
            }
			//格式转换
			List<String> aliasList=new ArrayList<>();
			for (int i=0;i<byMobile.size();i++){
                aliasList.add(Constant.ALIAS+byMobile.get(i));
			}
			if(pushBean.getAlert()==null||pushBean.getAlert().isEmpty()){
				return AsyncResult.error("内容不能为空");
			}
			//安卓--内容必填
		if(pushBean.getAudience()==0 && (pushBean.getAlert()!=null||!pushBean.getAlert().isEmpty())){
					if ((alias == null || alias.isEmpty()) && pushBean.getGoalPeople() == 0) {
						pushService.pushAndroid(pushBean);
						return AsyncResult.success();
					}
				 	if ((alias != null || !alias.isEmpty()) && pushBean.getGoalPeople() == 1) {
						pushService.pushAndroid(pushBean, aliasList);
						return AsyncResult.success();
					}
			else {
					return AsyncResult.error("请重试");
			}
		}

		//ios--内容必填
		if (pushBean.getAudience()==1 &&(pushBean.getAlert()!=null||!pushBean.getAlert().isEmpty())){
			    if((alias==null||alias.isEmpty()) && pushBean.getGoalPeople()==0){
                    pushService.pushIos(pushBean);
                    return AsyncResult.success();
                }
			if((alias!=null||!alias.isEmpty()) && pushBean.getGoalPeople()==1){
				pushService.pushIos(pushBean ,aliasList);
				return AsyncResult.success();
			}
			else {
				return AsyncResult.error("请重试");
			}
		}
		//全部--内容必填
		if(pushBean.getAudience()==2&&(pushBean.getAlert()!=null||!pushBean.getAlert().isEmpty())){
				pushService.pushAll(pushBean);
				return AsyncResult.success();
		}else {
				return AsyncResult.error("请重试");
		}
		}catch (Exception e){
			e.printStackTrace();
		}
		return asyncResult;
	}

}
