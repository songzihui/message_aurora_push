package com.jykj.reward.service.jdpush;


import com.jykj.reward.domain.Push;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 推送服务
 * 封装业务功能相关
 */
@Service
public class PushService {
	
	/** 一次推送最大数量 (极光限制1000) */
	private static final int max_size = 800;
	
	@Autowired
	private JPushService jPushService;
	
	
	/**
	 * 推送全部, 不支持附加信息
	 * @return
	 */
	public boolean pushAll(Push pushBean){
		return jPushService.pushAll(pushBean);
	}
	
	/**
	 * 推送全部ios
	 * @return
	 */
	public boolean pushIos(Push pushBean){
		return jPushService.pushIos(pushBean);
	}
	
	/**
	 * 推送ios 指定id
	 * @return
	 */
	public boolean pushIos(Push pushBean, List alias){

		return jPushService.pushIos(pushBean, alias);
	}
	
	/**
	 * 推送全部android
	 * @return
	 */
	public boolean pushAndroid(Push pushBean){
		return jPushService.pushAndroid(pushBean);
	}
	
	/**
	 * 推送android 指定alias
	 * @return
	 */
	public boolean pushAndroid(Push pushBean, List alias){
		return jPushService.pushAndroid(pushBean, alias);
	}

	/**
	 * 剔除无效registed
	 * @param registids
	 * @return
	 */
	private String[] checkRegistids(String[] registids) {
		List<String> regList = new ArrayList<String>(registids.length);
		for (String registid : registids) {
			if (registid!=null && !"".equals(registid.trim())) {
				regList.add(registid);
			}
		}
		return regList.toArray(new String[0]);
	}
	
}